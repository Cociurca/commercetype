import { createAppContainer } from 'react-navigation';
import { createBottomTabNavigator } from 'react-navigation-tabs';
import { createStackNavigator } from 'react-navigation-stack';
import createAnimatedSwitchNavigator from 'react-navigation-animated-switch';
import React from 'react';
import { Component } from 'react'
import { Provider } from 'react-redux';
import { PersistGate } from 'redux-persist/es/integration/react'
import { store, persistor } from '../redux/store';
import EStyleSheet from 'react-native-extended-stylesheet';
import { Platform, Dimensions, Image } from 'react-native';
import { Transition } from 'react-native-reanimated';
import AuthorizationScreen from '../screens/Auth/AuthorizationScreen';
import NewArrivals from '../screens/ECommerce/NewArrivals';
import images from '../assets/images';
import BackButton from '../components/navigation/BackButton';
import Cart from '../screens/ECommerce/Cart';
import ProductReview from '../screens/ECommerce/ProductReview';


const MainStack = createStackNavigator({
    NewArrivals: { screen: NewArrivals},
    Cart: { screen: Cart},
    ProductReview: {screen: ProductReview}
},{
    defaultNavigationOptions:{
        headerStyle:{
            borderBottomWidth:0
        },
        headerBackImage: BackButton,
        
    },
    headerBackTitleVisible:false
});

const MySwitch = createAnimatedSwitchNavigator(
    {
        Auth: AuthorizationScreen,
        MainStack: MainStack,
    },
    {
        transition: (
            <Transition.Together>
                <Transition.Out
                    type="fade"
                    durationMs={200}
                    interpolation="easeIn"
                />
                <Transition.In type="fade" durationMs={250} />
            </Transition.Together>
        ),
    }
);

let Nav = createAppContainer(MySwitch);

export default class Navigator extends Component {

    render() {
        return (
            <Provider store={store}>
                <PersistGate
                    loading={null}
                    persistor={persistor}
                >
                   <Nav/>
                </PersistGate>

            </Provider>
        )
    }
}

const width = Dimensions.get('screen').width;
EStyleSheet.build({
    $rem: Platform.select({
        ios: Platform.isPad ? width / 750 : width / 375
        ,
        android: width / 375
    })
  });