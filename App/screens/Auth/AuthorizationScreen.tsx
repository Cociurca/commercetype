import React, { Component } from 'react'
import { connect } from 'react-redux'
import { View, Text, SafeAreaView, StyleSheet, Image, Dimensions, StatusBar, TextInput, KeyboardAvoidingView, TouchableOpacity } from 'react-native'
import images from '../../assets/images'
import fonts from '../../assets/fonts'
import DisposableKeyboard from '../../components/DisposableKeyboard'
import NormalButton from '../../components/Buttons/NormalButton'
import { setRecommendation } from '../../redux/App/actions'
import generateName from '../../plugins/NameGenerator'
import { ECRecomendationData } from '../../components/Cards/ReccomendedCard'


class AuthorizationScreen extends Component<any> {

    constructor(props) {
        super(props)
        this.state = {}
    }

    handleLogin = () => {
        const { navigation } = this.props
        
        let recomms : Array<ECRecomendationData> = []
        const imgs = [images.recommendation_1, images.recommendation_2, images.recommendation_3, images.recommendation_4, images.recommendation_5, images.recommendation_6]
        imgs.map(e => {

            const recomm = {
                image : e,
                price : Math.floor(Math.random() * 1000),
                title : generateName(2)
            }
            recomms.push(recomm)
        })
        this.props.setRecommendation(recomms)
        navigation.navigate('MainStack')
    }

    render() {

        return (
            <View style={styles.mainView}>

                <StatusBar barStyle={'light-content'} />
                <Image style={[StyleSheet.absoluteFill, styles.coverImg]} source={images.login_bg} />
                <KeyboardAvoidingView style={styles.cover} behavior="padding" enabled>
                    <SafeAreaView style={[styles.mainView, styles.topContainer]}>
                        <Text style={styles.gettingStarted}>Getting Started</Text>
                        <Text style={styles.additionalInfo}>Signing up or login to see our top picks for you.</Text>
                    </SafeAreaView>
                    <DisposableKeyboard />
                    <View style={[styles.mainView, styles.popView]}>
                        <DisposableKeyboard />
                        <Text style={styles.loginTitle}>Enter your phone number</Text>
                        <Text style={styles.loginDescription}>Signing up or login to see our top picks for you.</Text>
                        <View style={styles.formHolder}>
                            <Text style={styles.numberTitle}>Mobile phone</Text>
                            <TextInput keyboardType={'number-pad'} textContentType={'telephoneNumber'} placeholder={'phone number'} placeholderTextColor={'#999999'} style={styles.textInput} />
                        </View>
                        <NormalButton onPress={this.handleLogin} title={'Get started'} style={styles.loginButton} />
                        <TouchableOpacity onPress={() => { }}>
                            <Text style={styles.forget}>Forget password?</Text>
                        </TouchableOpacity>
                    </View>

                </KeyboardAvoidingView>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    mainView: {
        flex: 1
    },
    coverImg: {
        resizeMode: 'cover',
        height: undefined,
        width: undefined
    },
    cover: {
        backgroundColor: '#0000004D',
        flex: 1,
        justifyContent: 'flex-end',
    },
    popView: {
        flex: 1.125,
        backgroundColor: 'white',
        borderTopLeftRadius: 13,
        borderTopRightRadius: 13,
        shadowColor: '#323247',
        shadowOffset: { width: 0, height: 16 },
        shadowRadius: 128,
        shadowOpacity: 0.24,
        paddingHorizontal: 25
    },
    topContainer: {
        justifyContent: 'center',
        alignItems: 'center'
    },
    gettingStarted: {
        fontFamily: fonts.sf_pro_display_semibold,
        fontSize: 28,
        color: 'white',
        maxWidth: '55%',
        marginTop: '-30%'
    },
    additionalInfo: {
        fontFamily: fonts.sf_pro_display_light,
        fontSize: 17,
        color: 'white',
        maxWidth: '48%',
        textAlign: 'center',
        marginTop: 14
    },
    loginTitle: {
        fontFamily: fonts.sf_pro_display_semibold,
        fontSize: 28,
        color: '#151522',
        marginTop: 37
    },
    loginDescription: {
        fontFamily: fonts.sf_pro_display_light,
        fontSize: 17,
        color: '#999999',
        marginTop: 24,
        maxWidth: '48%',
        alignSelf: 'center',
        textAlign: 'center'
    },
    formHolder: {
        marginTop: 40,
        height: 85
    },
    numberTitle: {
        fontFamily: fonts.sf_pro_display_semibold,
        fontSize: 15,
        color: '#151522',
    },
    textInput: {
        paddingHorizontal: 20,
        borderColor: '#E4E4E499',
        borderRadius: 5,
        borderWidth: 1,
        flex: 1,
        marginTop: 15,
        fontFamily: fonts.sf_pro_display_light,
        fontSize: 13,
        color: '#151522'
    },
    loginButton: {
        marginTop: 20,
        width: '100%'
    },
    forget: {
        fontFamily: fonts.sf_pro_display_regular,
        fontSize: 15,
        color: '#6979F8',
        alignSelf: 'center',
        marginTop: 20
    }
})

const mapStateToProps = (state) => ({

})

const mapDispatchToProps = {
    setRecommendation
}

export default connect(mapStateToProps, mapDispatchToProps)(AuthorizationScreen)
