
import React, { Component } from 'react'
import { connect } from 'react-redux'
import { View, SafeAreaView, ScrollView, StyleSheet, Text, Image } from 'react-native'
import { ECCartItem } from '../../redux/App/AppReducer'
import HeaderTitle from '../../components/navigation/HeaderTitle'
import fonts from '../../assets/fonts'
import CartItem from '../../components/Cards/CartItem'
import { TextInput } from 'react-native-gesture-handler'
import DeliveryMethodCell, { DeliveryMethod } from '../../components/Cards/DeliveryMethodCell'
import NormalButton from '../../components/Buttons/NormalButton'
import {removeFromCart} from '../../redux/App/actions'
import images from '../../assets/images'

interface Props {
    cart: Array<ECCartItem>,
    removeFromCart: (item:ECCartItem) => void
}
interface State {
    selectedDelivery: DeliveryMethod,
    discount:string
}

class Cart extends Component<Props, State> {
    deliveryMethods: DeliveryMethod[]
    deliveryNr: number;
    discountValue: string;


    static navigationOptions = ({ navigation }) => {
        return {
            headerTitle: () => <HeaderTitle title={'Cart'} />,
        };
    };

    constructor(props) {
        super(props)
        this.deliveryNr = new Date().getTime()
        this.discountValue = 'discount777'
        this.deliveryMethods = [
            {
                name: 'Standard Delivery',
                description: '2-3 days',
                price: 10.00
            },
            {
                name: 'Next Day Delivery',
                description: '',
                price: 25.00
            }
        ]
        this.state = { selectedDelivery: this.deliveryMethods[0], discount:'' }
    }

    render() {
        const { cart } = this.props


        if (cart.length == 0){
            return(
                <SafeAreaView style={{ flex: 1, overflow: 'visible', justifyContent:'center', alignItems:'center' }}>
                    <Text style={styles.orderTitle}>Cart is empty</Text>
                    <Text style={[styles.infoTxt,{flex:0, marginTop:15}]}>Keep track of our products and return</Text>
                    <Image source={images.shopping_cart} style={{marginTop:55,width:'50%', aspectRatio:1, height:undefined,}}/>
                </SafeAreaView>
            )
        }

        const { selectedDelivery, discount } = this.state

        let orderPrice = 0
        for (let index = 0; index < cart.length; index++) {
            const element = cart[index];
            orderPrice += element.count * element.item.price
        }
        let discountPrice = 0
        if (discount == this.discountValue){
            discountPrice = -(orderPrice * 0.13)
        }
        return (
            <SafeAreaView style={{ flex: 1, overflow: 'visible' }}>
                <ScrollView style={{ flex: 1, paddingHorizontal: 25 }}>
                    <Text style={styles.orderTitle}>Order number is {this.deliveryNr}</Text>
                    <View style={styles.listHolder}>
                        {
                            cart.map(item => {
                                return (
                                    <CartItem item={item} onRemove={() => this.props.removeFromCart(item)}/>
                                )
                            })
                        }
                    </View>
                    <Text style={styles.cappText}>Enter your discount code here</Text>
                    <TextInput style={styles.discountTextField} autoCapitalize={'none'} autoCorrect={false} onChangeText={(txt) => this.setState({discount:txt})} placeholder={'place discount here'} placeholderTextColor={'#E4E4E48C'} />
                    <View style={styles.deliveryHolder}>
                        {
                            this.deliveryMethods.map(e => {
                                return (
                                    <DeliveryMethodCell isSelected={e == selectedDelivery} delivery={e} onPress={() => this.setState({selectedDelivery:e})} />
                                )
                            })
                        }
                    </View>
                    <View style={styles.totalHolder}>
                        <View style={styles.rowPrice}>
                            <Text style={styles.infoTxt}>Order:</Text>
                            <Text style={[styles.infoTxt,{textAlign:'right'}]}>${orderPrice}</Text>
                        </View>
                        <View style={styles.rowPrice}>
                            <Text style={styles.infoTxt}>Discount:</Text>
                            <Text style={[styles.infoTxt,{textAlign:'right',color: discount == this.discountValue ? '#00C48C' : '#151522'}]}>${discountPrice}</Text>
                        </View>
                        <View style={styles.rowPrice}>
                            <Text style={styles.infoTxt}>Delivery:</Text>
                            <Text style={[styles.infoTxt,{textAlign:'right'}]}>${selectedDelivery.price}</Text>
                        </View>
                        <View style={styles.rowPrice}>
                            <Text style={[styles.cappText,{marginTop:0}]}>Total:</Text>
                            <Text style={[styles.infoTxt,{textAlign:'right'}]}>${selectedDelivery.price + discountPrice + orderPrice}</Text>
                        </View>
                    </View>
                    <NormalButton onPress={() => {}} title={'place order'} style={{marginTop:31, width:'100%'}}/>
                </ScrollView>
            </SafeAreaView>
        )
    }
}

const styles = StyleSheet.create({
    orderTitle: {
        fontFamily: fonts.sf_pro_display_semibold,
        fontSize: 17,
        color: '#222222',
        marginTop: 47
    },
    listHolder: {
        marginTop: 40
    },
    cappText: {
        fontFamily: fonts.sf_pro_display_semibold,
        fontSize: 15,
        color: '#151522',
        marginTop: 18
    },
    discountTextField: {
        flex: 1,
        height: 50,
        marginTop: 15,
        borderColor: '#E4E4E499',
        borderWidth: 1,
        paddingHorizontal: 20,
        borderRadius: 5,
        fontFamily: fonts.sf_pro_display_semibold,
        fontSize: 13,
        color: '#151522',
    },
    deliveryHolder: {
        marginTop: 30,
        flex:1
    },
    totalHolder:{
        marginTop:25,
    },
    rowPrice:{
        height:20,
        marginBottom:10,
        flexDirection:'row'
    },
    infoTxt:{
        fontFamily: fonts.sf_pro_display_light,
        fontSize: 15,
        color: '#151522',
        flex:1,
        textAlign:'left'
    }
})

const mapStateToProps = (state) => ({
    cart: state.AppReducer.cart
})

const mapDispatchToProps = {
    removeFromCart
}

export default connect(mapStateToProps, mapDispatchToProps)(Cart)
