import React, { Component } from 'react'
import { connect } from 'react-redux'
import { View, SafeAreaView, StyleSheet, Image, ScrollView, Text } from 'react-native'
import { ECProductData } from '../../components/Cards/ElementCard'
import images from '../../assets/images'
import fonts from '../../assets/fonts'
import Divider from '../../components/Other/Divider'
import SizeButton from '../../components/Buttons/SizeButton'
import ColorButton from '../../components/Buttons/ColorButton'
import NormalButton from '../../components/Buttons/NormalButton'
import Counter from '../../components/Other/Counter'
import ReccomendedList from '../../components/ECommerce/ReccomendedList'
import NavigationButtonWithImage from '../../components/navigation/NavigationButtonWithImage'
import { NavigationStackProp } from 'react-navigation-stack'
import {addToCart} from '../../redux/App/actions'
import { ECCartItem } from '../../redux/App/AppReducer'
import { ECRecomendationData } from '../../components/Cards/ReccomendedCard'

interface Props {
    currentProduct: ECProductData,
    navigation: NavigationStackProp,
    addToCart: (item:ECCartItem) => void,
    recommendations: Array<ECRecomendationData>
}
interface State {
    size: string,
    color: string,
    count: number,
    inCart: boolean
}

class ProductReview extends Component<Props, State> {
    colors: string[];
    sizes: string[];

    static navigationOptions = ({ navigation }) => {
        return {
            headerRight: () => <NavigationButtonWithImage onPress={navigation.getParam('onCart')} source={images.shopping_bag} />,
        };
    };

    constructor(props) {
        super(props)
        this.colors = ['#151522','#E4E4E4','#BE52F2','#343D63','#FFCF5C','#F2994A','#0084F4','#00C48C','#FF647C']
        this.sizes = ['XS', 'S', 'M', 'L', 'XL', 'XXL']
        this.state = { size: 'M', color: '#E4E4E4', count: 1 , inCart:false}
        this.props.navigation.setParams({ onCart: () => this.props.navigation.navigate('Cart') })
    }

    addToCart = () => {
        this.setState({inCart: true})
        let item = {
            item: this.props.currentProduct,
            size: this.state.size,
            color: this.state.color,
            count: this.state.count
        }
        this.props.addToCart(item)
    }

    render() {
        const { currentProduct } = this.props
        const {count,size,color, inCart} = this.state
        return (
            <SafeAreaView style={{ flex: 1, overflow: 'visible' }}>
                <ScrollView style={{ flex: 1, overflow: 'visible' }}>
                    <View style={styles.header}>
                        <Image style={styles.headerImg} source={images.review_image} />
                        <Text style={styles.price}>${currentProduct.price.toString()}</Text>
                    </View>
                    <View style={styles.mainContainer}>
                        <Text style={styles.type}>Shirts</Text>
                        <Text style={styles.title}>{currentProduct.title}</Text>
                        <Text style={styles.description}>{currentProduct.description}</Text>
                        <Divider />
                        <Text style={styles.type}>Size</Text>
                        <View style={styles.sizeHolder}>
                            {
                                this.sizes.map(sz => {
                                    return (
                                        <SizeButton onPress={() => this.setState({ size: sz })} size={sz} isSelected={size == sz} />
                                    )
                                })
                            }
                        </View>
                        <Text style={[styles.type, { marginTop: 0 }]}>Colors</Text>
                        <View style={styles.colorsHolder}>
                            {
                                this.colors.map(clr => {
                                    return (
                                        <ColorButton onPress={() => this.setState({ color: clr })} color={clr} isSelected={color == clr} />
                                    )
                                })
                            }
                        </View>
                        <Counter style={styles.counter} value={count} onIncrease={()=> this.setState({count: count + 1})} onDecrease={()=>this.setState({count: count > 1 ? count - 1 : 1})}/>
                        <NormalButton onPress={this.addToCart} title={'Add to cart'}/>
                    </View>
                    <ReccomendedList recommendations={this.props.recommendations}/>
                </ScrollView>

            </SafeAreaView>
        )
    }
}
const styles = StyleSheet.create({
    header: {
        width: '100%',
        aspectRatio: 375 / 440,
        justifyContent: 'flex-end'
    },
    headerImg: {
        ...StyleSheet.absoluteFillObject,
        resizeMode: 'cover',
        width: undefined,
        height: undefined
    },
    price: {
        fontFamily: fonts.sf_pro_display_semibold,
        fontSize: 48,
        color: 'white',
        marginBottom: 12,
        marginLeft: 25,
        textAlign: 'left',
    },
    mainContainer: {
        paddingHorizontal: 25
    },
    type: {
        marginTop: 32,
        color: "#151522",
        fontFamily: fonts.sf_pro_display_semibold,
        fontSize: 17,
    },
    title: {
        fontFamily: fonts.sf_pro_display_semibold,
        fontSize: 34,
        color: '#151522',
        marginTop: 15,
        textAlign: 'left',
        maxWidth: '90%'
    },
    description: {
        fontFamily: fonts.sf_pro_display_light,
        fontSize: 15,
        color: '#151522',
        marginVertical: 30,
        textAlign: 'left',
    },
    sizeHolder: {
        flexDirection: 'row',
        height: 35,
        marginRight: 20,
        marginVertical: 25,
        justifyContent: 'space-between'
    },
    colorsHolder: {
        flexDirection: 'row',
        flexWrap: 'wrap',
        marginVertical: 25,
    },
    counter:{
        marginTop:40,
        marginLeft:4,
        marginBottom:55,
    }
})

const mapStateToProps = (state) => ({
    currentProduct: state.AppReducer.currentProduct,
    recommendations: state.AppReducer.recommendations
})

const mapDispatchToProps = {
    addToCart
}

export default connect(mapStateToProps, mapDispatchToProps)(ProductReview)
