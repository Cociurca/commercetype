import React, { Component } from 'react'
import { connect } from 'react-redux'
import { View, ScrollView, StyleSheet, SafeAreaView, Text, FlatList } from 'react-native'
import NormalButton from '../../components/Buttons/NormalButton'
import HeaderTitle from '../../components/navigation/HeaderTitle'
import NavigationButtonWithImage from '../../components/navigation/NavigationButtonWithImage'
import images from '../../assets/images'
import CardHeader, { ECData } from '../../components/Cards/CardHeader'
import fonts from '../../assets/fonts'
import ReccomendedCard, {ECRecomendationData } from '../../components/Cards/ReccomendedCard'
import ReccomendedList from '../../components/ECommerce/ReccomendedList'
import ElementCard, { ECProduct } from '../../components/Cards/ElementCard'
import {setProduct} from '../../redux/App/actions'
import { NavigationStackProp } from 'react-navigation-stack'
import { ECCartItem } from '../../redux/App/AppReducer'
interface State {
    products: Array<ECProduct>
}
interface Props{
    recommendations: Array<ECRecomendationData>,
    navigation: NavigationStackProp,
    setProduct: (product: ECCartItem) => void
}

class NewArrivals extends Component<Props,State> {
    

    static navigationOptions = ({ navigation }) => {
        return {
            headerTitle: () => <HeaderTitle title={'New Arrivals'} style={{ textAlign: 'right', marginHorizontal: 18 }} />,
            headerRight: () => <NavigationButtonWithImage onPress={navigation.getParam('onCart')} source={images.shopping_bag} />,
        };
    };

    constructor(props) {
        super(props)
        this.props.navigation.setParams({ onCart: () => this.props.navigation.navigate('Cart') })
        let products: Array<ECProduct> = [];
        [images.Image, images.Image_1, images.Image_2, images.Image_3, images.Image_4, images.Image_5].map(e => {
            const rec = new ECProduct()
            rec.image = e
            products.push(rec)
        })

        this.state = {products:products}
    }


    renderItem = (e : any) => {
        const { item, index } = e
        return (
            <ElementCard data={item} index={index} onLike={() => {
                let prod = (item as ECProduct)
                prod.isLiked = !prod.isLiked
                this.setState({products:this.state.products})
            }}
            onPress={() => {
                this.props.setProduct(item)
                this.props.navigation.navigate('ProductReview')
            }}/>
        )
    }

    render() {
        

        return (
            <SafeAreaView style={{ flex: 1 }}>
                <ScrollView showsVerticalScrollIndicator={false} style={{ flex: 1, overflow:'visible' }}>
                    <CardHeader data={new ECData()} image={images.collection_mask} style={styles.cardHeader} />
                    <ReccomendedList recommendations={this.props.recommendations}/>
                    <FlatList
                        style={{ overflow: 'visible' }}
                        contentContainerStyle={{ paddingTop: 28,}}
                        numColumns={2}
                        data={this.state.products}
                        renderItem={this.renderItem}
                        keyExtractor={(item, index) => item.title + index.toString()}
                    />

                </ScrollView>
            </SafeAreaView>
        )
    }
}

const styles = StyleSheet.create({
    cardHeader: {
        width: '100%',
        aspectRatio: 375 / 510
    },
})

const mapStateToProps = (state) => ({
    recommendations: state.AppReducer.recommendations
})

const mapDispatchToProps = {
    setProduct
}

export default connect(mapStateToProps, mapDispatchToProps)(NewArrivals)
