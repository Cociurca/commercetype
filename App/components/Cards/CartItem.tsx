import React, { ReactElement } from 'react'
import { ECCartItem } from '../../redux/App/AppReducer'
import { View, StyleSheet, ViewStyle, Image, Text, TouchableOpacity } from 'react-native'
import Divider from '../Other/Divider'
import fonts from '../../assets/fonts'
import ColorButton from '../Buttons/ColorButton'
import images from '../../assets/images'

interface Props {
    item: ECCartItem,
    style?: ViewStyle,
    onRemove : ()=>void
}

export default function CartItem({ item, style,onRemove }: Props): ReactElement {
    return (
        <View style={[styles.holder, style]}>
            <View style={styles.content}>
                <Image style={styles.img} source={item.item.image} />
                <View style={styles.mainInfo}>
                    <Text numberOfLines={0} style={styles.title}>{item.item.title}</Text>
                    <View style={styles.otherInfo}>
                        <Text style={styles.price}>Size:<Text style={styles.otherText}> {item.size}</Text></Text>
                        <View style={{ flexDirection: 'row' }}>
                            <Text style={styles.price}>Color:</Text>
                            <ColorButton style={styles.color} disabled={true} isSelected={false} color={item.color} />
                        </View>

                        <Text style={styles.price}>Quantity:<Text style={styles.otherText}> x{item.count}</Text></Text>
                    </View>
                </View>
                <View>
                    <Text style={[styles.price, { marginRight: 20 }]}>
                        ${item.count * item.item.price}
                    </Text>
                    <View style={styles.trashicon}>
                        <TouchableOpacity onPress={onRemove}>
                            <Image style={styles.trash} source={images.trash_2}/>
                        </TouchableOpacity>
                    </View>
                </View>

            </View>
            <Divider style={styles.divider} />
        </View>
    )
}

const styles = StyleSheet.create({
    holder: {
        aspectRatio: 325 / 91,
        flex: 1,
    },
    content: {
        flexDirection: 'row',
        flex: 1,
    },
    divider: {
        marginVertical: 15,
        width: '100%',
    },
    img: {
        height: '100%',
        aspectRatio: 1,
        borderRadius: 4,
        resizeMode: 'cover',
        width: undefined,
    },
    mainInfo: {
        flex: 1,
        marginHorizontal: 20,

    },
    trashicon:{
        flex:1,
        justifyContent:'flex-end'
    },
    trash:{
        width:23,
        height:23,
        resizeMode:'contain',
        alignSelf:'flex-end'
    },
    otherInfo: {
        flex: 1,
        justifyContent: 'flex-end',
        alignItems: 'flex-start'
    },
    otherText: {
        color: '#999999',
        fontFamily: fonts.sf_pro_display_regular,
        fontSize: 11,
    },
    title: {
        color: '#151522',
        fontFamily: fonts.sf_pro_display_semibold,
        fontSize: 15,
        height: 20
    },
    price: {
        color: '#151522',
        fontFamily: fonts.sf_pro_display_regular,
        fontSize: 14,
        marginRight: 3
    },
    color: {
        height: 18,
        marginHorizontal: undefined,
        marginVertical: undefined,
        borderWidth: 1.5
    }
})
