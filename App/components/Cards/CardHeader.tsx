import React from 'react'
import { View, ImageSourcePropType, StyleSheet, Image, ViewStyle, Text } from 'react-native'
import fonts from '../../assets/fonts'
import images from '../../assets/images'
import Divider from '../Other/Divider'
import InfoView from '../Info/InfoView'

interface ECommerceData {
    likes: number,
    comments: number,
    saves: number,
    title: string,
}

export class ECData implements ECommerceData {
    likes = Math.floor(Math.random() * 100)
    comments = Math.floor(Math.random() * 100)
    saves = Math.floor(Math.random() * 100)
    title = ''
}


interface Props {
    image: ImageSourcePropType,
    data: ECommerceData,
    onShare?: () => void,
    onComment?: () => void,
    onLike?: () => void,
    onSave?: () => void,
    style: ViewStyle
}

const CardHeader: React.FC<Props> = ({ image, data, onComment, onLike, onSave, onShare, style }: Props) => {
    return (
        <View style={style}>
            <View style={styles.topView}>
                <Image style={[StyleSheet.absoluteFill, styles.image]} source={image} />
                <View style={styles.imageHeader}>
                    <Image style={styles.cart} source={images.shopping_bag_white} />
                    <Text style={styles.cartTxt}>SALE</Text>
                </View>
                <View style={{ flex: 1, justifyContent: 'flex-end' }}>
                    <Text style={styles.title}>Get 20% off fall collection</Text>
                </View>
            </View>
            <View style={styles.bottomBar}>
                <View style={{ flexDirection: 'row' }}>
                    <InfoView style={styles.infoV} text={data.likes.toString()} icon={images.heart} />
                    <InfoView style={styles.infoV} text={data.comments.toString()} icon={images.message_circle} />
                    <InfoView style={styles.infoV} text={data.saves.toString()} icon={images.bookmark} />
                </View>
                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'flex-end' }}>
                    <Image style={styles.infoV} source={images.share_2} />
                </View>
            </View>
            <Divider />
        </View>
    )
}

const styles = StyleSheet.create({
    bottomBar: {
        height: 60,
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',
        paddingHorizontal: 25,
        paddingVertical: 15
    },
    shareIcon: {
        width: 28,
        height: 28,
        resizeMode: 'contain'
    },
    infoV: {
        height: '100%',
        marginRight: 15
    },
    topView: {
        flex: 1,
        paddingHorizontal: 25
    },
    image: {
        width: undefined,
        height: undefined,
        resizeMode: 'cover'
    },
    imageHeader: {
        marginTop: 25,
        flexDirection: 'row',
        alignItems: 'center'
    },
    cart: {
        width: 28,
        height: 28,
        resizeMode: 'contain'
    },
    cartTxt: {
        fontFamily: fonts.sf_pro_display_semibold,
        fontSize: 17,
        color: 'white',
        marginLeft: 9,
        alignSelf: 'center'
    },
    title: {
        fontFamily: fonts.sf_pro_display_semibold,
        fontSize: 34,
        color: 'white',
        marginBottom: 47,
        textAlign: 'left',
        maxWidth: '55%'
    }

})

export default CardHeader
