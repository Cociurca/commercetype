import React, { ReactElement } from 'react'
import { View, StyleSheet, Image, TouchableOpacity, Text } from 'react-native'
import images from '../../assets/images'
import fonts from '../../assets/fonts'


export type DeliveryMethod = {
    name:string,
    description:string,
    price:number
}

interface Props {
    isSelected: boolean,
    delivery: DeliveryMethod,
    onPress: ()=>void
}

export default function DeliveryMethodCell({isSelected,delivery,onPress}: Props): ReactElement {
    let description = ''
    if (delivery.description != ''){
        description += delivery.description + ', '
    }
    return (
        <TouchableOpacity style={styles.holder} onPress={onPress}>
            <View style={[styles.selectHolder , isSelected ? {
                shadowOpacity: 0.06,
                shadowRadius: 2,
                shadowOffset: { width: 0, height: 2 },
                shadowColor: '#323247',
                borderWidth:0,
                backgroundColor:'#6979F8'
            } : {}]}>
                {
                    isSelected ? 
                    <Image style={styles.iconSelect} source={images.check}/>
                    :
                    null
                }
            </View>
            <View style={styles.mainInfo}>
                <Text numberOfLines={0} style={styles.title}>{delivery.name}</Text>
            <Text style={styles.description}>({description}${delivery.price})</Text>
            </View>
        </TouchableOpacity>
    )
}

const styles = StyleSheet.create({
    holder:{
        aspectRatio:325/77,
        width:'100%',
        flexDirection:'row',
        marginBottom:35,
        borderColor: '#E4E4E499',
        borderWidth:1,
        borderRadius:10
    },
    selectHolder:{
        width:'8%',
        aspectRatio:24/22,
        borderColor:'#E4E4E4',
        borderWidth:1,
        borderRadius:6,
        marginLeft:14,
        marginTop:13,
        justifyContent:'center',
        alignItems:'center'
    },
    iconSelect:{
        flex:0.9,
        resizeMode:'contain'
    },
    mainInfo:{
        marginLeft:19,
        marginTop:13,
    },
    title: {
        color: '#151522',
        fontFamily: fonts.sf_pro_display_semibold,
        fontSize: 15,
        height:20
    },
    description:{
        color: '#999999',
        fontFamily: fonts.sf_pro_display_light,
        fontSize: 15,
        flex:1,
        maxWidth:'60%'
    }

})
