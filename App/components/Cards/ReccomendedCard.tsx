import React, { ReactElement, constructor } from 'react'
import { View, ImageSourcePropType, StyleSheet, ViewStyle, Text, Image } from 'react-native'
import images from '../../assets/images'
import fonts from '../../assets/fonts';
import generateName from '../../plugins/NameGenerator';


export interface ECRecomendationData {
    title: string,
    price: number,
    image: ImageSourcePropType,
}

interface Props {
    data: ECRecomendationData,
    onPress?: () => void,
    style?: ViewStyle
}

export default function ReccomendedCard({ data, onPress, style }: Props): ReactElement {
    return (
        <View style={[styles.holder, style]}>
            <Image style={styles.image} source={data.image}/>
            <Text style={styles.title}>{data.title}</Text>
            <Text style={styles.price}>${data.price.toString()}</Text>
        </View>
    )
}

const styles = StyleSheet.create({
    holder: {
        aspectRatio: 150 / 252,
        marginHorizontal: 7.5,
        height: '100%',
    },
    price:{
        fontFamily: fonts.sf_pro_display_regular,
        fontSize:11,
        color:"#666666"
    },
    title:{
        fontFamily: fonts.sf_pro_display_regular,
        fontSize:13,
        color:"#151522",
        marginBottom:11
    },
    image:{
        flex: 1,
        width: undefined,
        height: undefined,
        marginBottom:11,
        resizeMode:'cover',
        borderRadius:10
    }
})