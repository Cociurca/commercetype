import React, { ReactElement } from 'react'
import { View, StyleSheet, ImageSourcePropType, Image, Text, TouchableOpacity } from 'react-native'
import images from '../../assets/images';
import generateName from '../../plugins/NameGenerator';
import fonts from '../../assets/fonts';


export type ECProductData = {
    title: string,
    price: number,
    image: ImageSourcePropType,
    isLiked: boolean,
    description: string
}

export class ECProduct implements ECProductData {
    title: string;
    price: number;
    image: ImageSourcePropType;
    isLiked: boolean;
    description: string;
    constructor() {
        this.image = images.recommendation_1
        this.price = Math.floor(Math.random() * 1000)
        this.title = generateName(3)
        this.isLiked = false
        this.description = 'Enjoy the beauty of italian cotton all over your body. This item will fit your body and warm you up all over and during spring. This item will fit your body and warm you up all over and during spring. \n\nAnd over and over again, this is the text.'
    }
}


interface Props {
    data: ECProductData,
    index: number,
    onLike?: () => void,
    onPress?: () => void
}

export default function ElementCard({ data, index , onLike,onPress}: Props): ReactElement {
    return (
        <TouchableOpacity onPress={onPress} style={[styles.holder, { marginLeft: index % 2 == 0 ? 25 : 17.5, marginRight: index % 2 == 0 ? 17.5 : 25 }]}>
            <Image style={styles.image} source={data.image} />
            <TouchableOpacity style={styles.likeHolder} onPress={onLike}>
                <Image style={styles.iconH} source={data.isLiked ? images.heart_on : images.heart}/>
            </TouchableOpacity>
            <Text style={styles.title}>{data.title}</Text>
            <View style={styles.priceHolder}>
                <Text style={styles.price}>${data.price.toString()}</Text>
                <Text style={styles.priceSecond}>${Math.floor(data.price * 0.8).toString()}</Text>
            </View>
        </TouchableOpacity>
    )
}

const styles = StyleSheet.create({
    holder: {
        flex: 1,
        marginVertical: 9,
        aspectRatio: 150 / 230,
        marginHorizontal: 17.5,
        backgroundColor: 'white',
        borderRadius: 10,
        shadowRadius: 8,
        shadowColor: '#323247',
        shadowOffset: { width: 0, height: 16 },
        shadowOpacity: 0.08
    },
    priceHolder: {
        marginBottom: 12,
        flexDirection: 'row',
    },
    price: {
        fontFamily: fonts.sf_pro_display_regular,
        fontSize: 15,
        color: "#151522",
        marginLeft: 15
    },
    priceSecond: {
        fontFamily: fonts.sf_pro_display_regular,
        fontSize: 13,
        color: "#999999",
        marginLeft: 10,
        alignSelf: 'center'
    },
    title: {
        fontFamily: fonts.sf_pro_display_regular,
        fontSize: 13,
        color: "#151522",
        marginBottom: 8,
        marginLeft: 15
    },
    image: {
        flex: 1,
        width: undefined,
        height: undefined,
        marginBottom: 18,
        resizeMode: 'cover',
        borderRadius: 10
    },
    likeHolder: {
        position: 'absolute',
        width: 25,
        height: 25,
        top: 8,
        right: 9,
        borderRadius: 17.5,
        backgroundColor: 'white',
        justifyContent: 'center',
        alignItems: 'center'
    },
    iconH: {
        flex: 1,
        resizeMode: 'contain'
    }
})
