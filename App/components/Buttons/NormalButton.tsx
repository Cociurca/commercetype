import React, { PureComponent } from 'react'
import { TouchableOpacity, ViewStyle, TouchableOpacityProps, StyleSheet, TextProps, Text } from 'react-native'
import fonts from '../../assets/fonts'

interface Props {
    title?: string,
    style?: ViewStyle,
    titleStyle?: TextProps,
    onPress: () => void,
    disabled?:boolean
}


export default class NormalButton extends PureComponent<Props> {
    render() {
        return (
            <TouchableOpacity disabled={this.props.disabled} style={[styles.holder, this.props.style]} onPress={this.props.onPress} >
                <Text style={[styles.title, this.props.titleStyle]}>{this.props.title}</Text>
            </TouchableOpacity>
        )
    }
}

const styles = StyleSheet.create({
    holder: {
        aspectRatio: 325 / 50,
        backgroundColor: '#6979F8',
        borderRadius: 5,
        shadowColor: '#323247',
        shadowOpacity: 0.08,
        shadowRadius: 4,
        textShadowOffset: { width: 0, height: 4 },
        justifyContent: 'center',
        alignItems: 'center',
    },
    title: {
        color: 'white',
        fontFamily: fonts.sf_pro_display_light,
        fontSize: 16
    }
})
