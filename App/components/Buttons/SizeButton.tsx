import React, { ReactElement } from 'react'
import { TouchableOpacity, StyleSheet, Text } from 'react-native'
import fonts from '../../assets/fonts'

interface Props {
    size: string,
    onPress: () => void,
    isSelected: boolean
}

export default function SizeButton({ size, onPress, isSelected }: Props): ReactElement {
    return (
        <TouchableOpacity style={[styles.sizeCell, { backgroundColor: isSelected ? '#6979F8' : undefined }, isSelected ? {
            shadowColor: '#323247',
            shadowOpacity: 0.09,
            shadowRadius: 4,
            shadowOffset: { width: 0, height: 4 }
        } : {}]} onPress={onPress}>
            <Text style={[styles.sizeTxt, { color: isSelected ? 'white' : '#E4E4E4B3' }]}>{size}</Text>
        </TouchableOpacity>
    )
}

const styles = StyleSheet.create({
    sizeCell: {
        height: '100%',
        borderWidth: 1,
        borderColor: '#E4E4E499',
        borderRadius: 6,
        aspectRatio: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    sizeTxt: {
        fontFamily: fonts.sf_pro_display_semibold,
        fontSize: 15,
    }
})