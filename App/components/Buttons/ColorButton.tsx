import React, { ReactElement } from 'react'
import { TouchableOpacity, StyleSheet, Text, ViewStyle } from 'react-native'
import fonts from '../../assets/fonts'

interface Props {
    style?:ViewStyle,
    color: string,
    onPress?: () => void,
    isSelected: boolean,
    disabled?:boolean
}

export default function ColorButton({ style,color, onPress, isSelected, disabled }: Props): ReactElement {
    return (
        <TouchableOpacity disabled={disabled} style={[styles.sizeCell, { backgroundColor: color, borderColor: isSelected ? '#6979F8' : 'white' },style]} onPress={onPress}/> 
    )
}

const styles = StyleSheet.create({
    sizeCell: {
        height: 36,
        marginHorizontal:11,
        marginVertical:5.5,
        borderWidth: 3,
        borderRadius: 100,
        aspectRatio: 1,
        shadowColor:'#323247',
        shadowOpacity:0.06,
        shadowRadius:4,
        shadowOffset:{width:0,height:2}
    },
})