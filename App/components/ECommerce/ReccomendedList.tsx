import React, { ReactElement } from 'react'
import { ScrollView, StyleSheet, View, Text } from 'react-native'
import images from '../../assets/images'
import ReccomendedCard, { ECRecomendationData } from '../Cards/ReccomendedCard'
import fonts from '../../assets/fonts'

interface Props {
    recommendations: ECRecomendationData[]
}

export default function ReccomendedList({recommendations }: Props): ReactElement {

    return (
        <View>
            <Text style={styles.recomended}>Recommended for you</Text>
            <ScrollView horizontal showsHorizontalScrollIndicator={false} style={styles.scroll} contentContainerStyle={styles.container}>
                {
                    recommendations.map(rec => {
                        return (
                            <ReccomendedCard data={rec} />
                        )
                    })
                }
            </ScrollView>
        </View>

    )
}

const styles = StyleSheet.create({
    scroll: {
        width: '100%',
        height: 250,
        marginTop: 20
    },
    container: {
        paddingHorizontal: 17.5
    },
    recomended: {
        color: '#151522',
        fontFamily: fonts.sf_pro_display_semibold,
        fontSize: 17,
        marginLeft: 25,
        marginTop: 50
    }
})