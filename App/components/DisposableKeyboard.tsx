import React, { PureComponent } from 'react'
import { TouchableWithoutFeedback, Keyboard , StyleSheet, View} from 'react-native'

interface Props {
    
}

export default class DisposableKeyboard extends PureComponent<Props> {
    render() {
        return (
            <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
                <View style={StyleSheet.absoluteFill} />
            </TouchableWithoutFeedback>
        )
    }
}
