
import React, { ReactElement } from 'react'
import { View, TouchableOpacity, TouchableOpacityProps, StyleSheet, Image, ImageProps, ImageSourcePropType, Text } from 'react-native'
import fonts from '../../assets/fonts'
import { connect } from 'react-redux'
import { ECCartItem } from '../../redux/App/AppReducer'


interface Props {
    onPress: () => {},
    holderStyle?: TouchableOpacityProps,
    imageProps?: ImageProps,
    source: ImageSourcePropType,
    cart: Array<ECCartItem>
}

function NavigationButtonWithImage({ onPress, holderStyle, source, imageProps, cart }: Props): ReactElement {
    return (
        <TouchableOpacity style={[styles.holder, holderStyle]} onPress={onPress}>
            <Image style={[styles.img, imageProps]} source={source} />
            {
                cart.length > 0 ?
                    <View style={styles.badge}>
                        <Text style={styles.badgetxt}>{cart.length}</Text>
                    </View>
                    :
                    null
            }
        </TouchableOpacity>
    )
}

const mapStateToProps = (state) => ({
    cart: state.AppReducer.cart
})

const mapDispatchToProps = {

}




const styles = StyleSheet.create({
    holder: {
        width: 28,
        height: 28,
        marginRight: 16,
        justifyContent: 'center',
        alignItems: 'center'
    },
    img: {
        resizeMode: 'contain',
        flex: 1
    },
    badge: {
        position: 'absolute',
        width: 15,
        height: 15,
        backgroundColor: '#FF647C',
        borderRadius: 7.5,
        right: -2,
        top: -3,
        justifyContent:'center',
        alignItems:'center'
    },
    badgetxt: {
        fontFamily: fonts.sf_pro_display_semibold,
        fontSize: 9,
        color: 'white'
    }
})

export default connect(mapStateToProps, mapDispatchToProps)(NavigationButtonWithImage)