import React, { ReactElement } from 'react'
import { Text, StyleSheet, TextStyle } from 'react-native'
import fonts from '../../assets/fonts'

interface Props {
    style?:TextStyle,
    title:string
}

export default function HeaderTitle({style,title}: Props): ReactElement {
    return (
        <Text style={[styles.title,style]}>
            {title}
        </Text>
    )
}

const styles = StyleSheet.create({
    title:{
        color:'#151522',
        fontFamily:fonts.sf_pro_display_semibold,
        fontSize:17,
        flex:1,
        textAlign:'center'
    }
})