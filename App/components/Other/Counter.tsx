import React, { ReactElement } from 'react'
import { View, ViewStyle, StyleSheet, Image, TouchableOpacity, Text } from 'react-native'
import images from '../../assets/images'
import fonts from '../../assets/fonts'

interface Props {
    style: ViewStyle,
    value: number,
    onIncrease: () => void,
    onDecrease: () => void
}

export default function Counter({ style, value, onIncrease, onDecrease }: Props): ReactElement {
    return (
        <View style={[styles.hodler, style]}>
            <TouchableOpacity style={styles.step} onPress={onIncrease}>
                <Image style={styles.img} source={images.plus} />
            </TouchableOpacity>
            <View style={[styles.step, styles.count]}>
                <Text style={styles.text}>{value}</Text>
            </View>
            <TouchableOpacity style={styles.step} onPress={onDecrease}>
                <Image style={styles.img} source={images.minus} />
            </TouchableOpacity>
        </View>
    )
}

const styles = StyleSheet.create({
    hodler: {
        aspectRatio: 100 / 32,
        flexDirection: 'row',
        alignItems: 'center',
        height: 38
    },
    count: {
        height: '100%',
        marginHorizontal: 10
    },
    step: {
        aspectRatio: 1,
        height: '75%',
        backgroundColor: 'white',
        shadowColor: '#323247',
        borderRadius: 8,
        shadowOpacity: 0.06,
        shadowRadius: 2,
        shadowOffset: { width: 0, height: 2 },
        alignItems: 'center',
        justifyContent: 'center'
    }, img: {
        flex: 1,
        resizeMode: 'contain'
    },
    text:{
        color:'#6979F8',
        fontFamily: fonts.sf_pro_display_semibold,
        fontSize: 15,

    }
})
