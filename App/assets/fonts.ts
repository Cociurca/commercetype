const fonts = {
  sf_pro_display_black: 'SFProDisplay-Black',
  sf_pro_display_blackitalic: 'SFProDisplay-BlackItalic',
  sf_pro_display_bold: 'SFProDisplay-Bold',
  sf_pro_display_bolditalic: 'SFProDisplay-BoldItalic',
  sf_pro_display_heavy: 'SFProDisplay-Heavy',
  sf_pro_display_heavyitalic: 'SFProDisplay-HeavyItalic',
  sf_pro_display_light: 'SFProDisplay-Light',
  sf_pro_display_lightitalic: 'SFProDisplay-LightItalic',
  sf_pro_display_medium: 'SFProDisplay-Medium',
  sf_pro_display_mediumitalic: 'SFProDisplay-MediumItalic',
  sf_pro_display_regular: 'SFProDisplay-Regular',
  sf_pro_display_regularitalic: 'SFProDisplay-RegularItalic',
  sf_pro_display_semibold: 'SFProDisplay-Semibold',
  sf_pro_display_semibolditalic: 'SFProDisplay-SemiboldItalic',
  sf_pro_display_thin: 'SFProDisplay-Thin',
  sf_pro_display_thinitalic: 'SFProDisplay-ThinItalic',
  sf_pro_display_ultralight: 'SFProDisplay-Ultralight',
  sf_pro_display_ultralightitalic: 'SFProDisplay-UltralightItalic'
}

export default fonts
