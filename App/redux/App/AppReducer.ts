import { ECProduct, ECProductData } from "../../components/Cards/ElementCard";
import { SET_PRODUCT, ADD_TO_CART, SET_RECOMMENDATION, REMOVE_FROM_CART } from "./types";
import { ECRecomendationData } from "../../components/Cards/ReccomendedCard";

export type ECCartItem = {
    item: ECProductData,
    color: string,
    size: string,
    count: number,
}

const initialState = {
    currentProduct: <ECProductData>{},
    cart: Array<ECCartItem>(),
    recommendations: Array<ECRecomendationData>()
};


const AppReducer = (state = initialState, action : any) => {

    switch (action.type) {
        case SET_PRODUCT:
            let prod = <ECProductData>action.product
            return {...state , currentProduct: prod}
        case SET_RECOMMENDATION:
            let recomm = <ECRecomendationData[]>action.recomendations
            return {...state, recommendations:recomm}
        case ADD_TO_CART:
            let produs = <ECCartItem>action.product
            return {...state , cart: [...state.cart,produs]}
        case REMOVE_FROM_CART:
            let removed = <ECCartItem>action.product
            let indexremov = state.cart.indexOf(removed)
            let arr = state.cart
            let newarr = arr.splice(indexremov,1)
            console.log(arr);
            
            return {...state , cart: [...arr]}
        default:
            return state;
    }
};

export default AppReducer;