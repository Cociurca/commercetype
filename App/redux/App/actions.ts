import { SET_PRODUCT, ADD_TO_CART, SET_RECOMMENDATION, REMOVE_FROM_CART } from "./types";
import { ECProductData } from "../../components/Cards/ElementCard";
import { ECCartItem } from "./AppReducer";
import { ECRecomendationData } from "../../components/Cards/ReccomendedCard";


export const setProduct = (product: ECProductData) => (
    {
      type: SET_PRODUCT,
      product
    }
  );

  export const setRecommendation = (recomendations: ECRecomendationData[]) => (
    {
      type: SET_RECOMMENDATION,
      recomendations
    }
  );

export const addToCart = (product: ECCartItem) => (
  {
    type: ADD_TO_CART,
    product,
  }
);

export const removeFromCart = (product: ECCartItem) => (
  {
    type: REMOVE_FROM_CART,
    product,
  }
);